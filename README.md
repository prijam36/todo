#ToDo
Task List
Here are the tasks completed for this project:
Fetch a list of todos from https://jsonplaceholder.typicode.com/todos and render the list in the UI.
Filter the list on UI based on todo complete state.
Toggle todo state on click on checkbox.
Swipe the todo to delete the todo from the list.
On stats page render out the count of completed and in-completed todo.

#Approach and Explanation
For this ToDo application, I chose to use the Flutter framework for building the user interface and GetX package for state management. GetX is a powerful package that provides a simple and intuitive way to manage state, dependencies, and navigation in Flutter applications.

State Management
I used the GetX package for state management in this application. GetX offers a variety of state management solutions including observables, reactive state management, and dependency injection. In this application, I utilized GetX's Rx classes for reactive state management. Specifically, I used RxBool, RxInt, and RxList to manage boolean, integer, and list state respectively. These reactive variables allow the UI to reactively update whenever there are changes to the underlying data.

UI Design
The UI of the ToDo application is designed using Flutter's built-in widgets along with some custom widgets. The main screen consists of an app bar with a dropdown menu for filtering todos based on their completion state. Below the app bar, there is a row of two widgets displaying the counts of completed and remaining todos respectively. The main body of the screen is a scrollable list of todos, each represented by a ListTile widget. The todos can be checked or unchecked using a Checkbox widget, and can be deleted by swiping them horizontally.

Data Fetching
To fetch the list of todos, I made an HTTP request to the JSONPlaceholder API. Upon receiving the response, I parsed the JSON data and populated the list of todos. Additionally, I maintained separate counts for completed and remaining todos, and filtered the list based on the selected filter criteria.

Filtering
The application allows the user to filter the list of todos based on their completion state. The available filter options are "All", "Completed", and "Remaining". When a filter option is selected from the dropdown menu, the list of todos is updated accordingly.

Handling Todo State Changes
Whenever a todo's completion state is toggled by the user, the checkComplete method in the HomeController is called to update the todo's completion state. This triggers a refresh of the counts and the filtered list, ensuring that the UI reflects the latest changes.

Conclusion
Overall, this ToDo application provides a simple yet effective way to manage and organize tasks. It leverages the power of Flutter and GetX to deliver a responsive and intuitive user experience. With its clean and organized interface, users can easily view, filter, and interact with their todo list, making task management a breeze.

#Git Pull vs Git Fetch
Git Pull and Git Fetch are both Git commands used to update the local repository with changes from a remote repository. However, they have different behaviors and purposes.

Git Fetch
git fetch downloads the latest changes from a remote repository to the local repository.
It updates the remote tracking branches, but does not merge the changes into the local branches.
The fetched changes can be viewed using git log origin/master (replace origin/master with the appropriate remote branch name).
git fetch is a safe operation and does not affect the local working directory or staging area.
It is typically used to fetch changes from the remote repository without automatically merging them into the local branches.

Git Pull
git pull is a combination of two operations: git fetch followed by git merge.
It fetches the latest changes from a remote repository and automatically merges them into the current local branch.
Unlike git fetch, git pull directly updates the local working directory and staging area with the fetched changes.
git pull is a more convenient way to update the local repository, but it can potentially cause conflicts if there are    conflicting changes between the local and remote branches.

Key Differences
git fetch only downloads the changes from the remote repository, while git pull also incorporates those changes into the local branch.
git fetch is safer as it does not modify the local working directory or staging area, whereas git pull directly updates the local repository.
git fetch allows for more control over which changes to merge and when to merge them, while git pull automatically merges the changes, which may lead to unexpected conflicts.

Conclusion
In summary, git fetch is a safe way to fetch the latest changes from a remote repository without automatically merging them into the local branch, while git pull fetches and merges the changes into the local branch in one operation. The choice between the two commands depends on the desired workflow and level of control over the merging process.