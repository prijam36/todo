import 'package:http/http.dart' as http;

class NetworkHelper {
  static final NetworkHelper _helper = NetworkHelper._internal();

  NetworkHelper._internal();

  factory NetworkHelper() => _helper;

  Future<http.Response> getRequest(String endPoint) async {
    try {
      Uri url = Uri.parse(endPoint);
      http.Response response = await http.get(url);
      return response;
    } on Exception catch (e) {
      throw Exception('Something went wrong ${e.toString()}');
    }
  }
}
