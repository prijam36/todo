import 'dart:convert';

import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:testbb/model.dart';
import 'package:testbb/network_helper.dart';
import 'package:testbb/common.dart';

import 'package:http/http.dart' as http;

class HomeController extends GetxController {
  final NetworkHelper _networkHelper = NetworkHelper();
  final List<ToDoModel> list = [];
  final RxList<ToDoModel> filteredList = <ToDoModel>[].obs;
  final RxBool _loading = true.obs;
  final RxInt completedCount = 0.obs;
  final RxInt remaining = 0.obs;

  List<int> deleteIndex = [];

  get loading => _loading.value;

  Future getList() async {
    try {
      http.Response response =
          await _networkHelper.getRequest(UrlCommon.getPoint);

      if (response.statusCode == HttpStatus.ok) {
        List<dynamic> initialList = jsonDecode(response.body);
        completedCount.value = 0;
        remaining.value = 0;
        for (var item in initialList) {
          ToDoModel model = ToDoModel.fromJson(item);
          list.add(model);
          filteredList.add(model);
          if (model.completed!) {
            completedCount.value++;
          } else {
            remaining.value++;
          }
        }
        _loading.value = false;
      } else {
        throw Exception('Something went wrong');
      }
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }

  void refreshCounts() {
    completedCount.value = filteredList.where((task) => task.completed!).length;
    remaining.value = filteredList.where((task) => !task.completed!).length;
  }

  void checkComplete(bool? complete, ToDoModel toDoModel) {
    toDoModel.completed = complete!;
    refreshCounts();
    filteredList.refresh();
  }

  void filterList(String filterCriteria) {
    switch (filterCriteria) {
      case 'Completed':
        filteredList.clear();
        for (var item in list) {
          if (item.completed!) {
            filteredList.add(item);
          }
        }
        filteredList.refresh();
        break;
      case 'Remaining':
        filteredList.clear();
        for (var item in list) {
          if (!item.completed!) {
            filteredList.add(item);
          }
        }
        filteredList.refresh();
        break;
      default:
        filteredList.clear();
        filteredList.addAll(list);
        filteredList.refresh();
        break;
    }
  }

  @override
  void onInit() {
    getList();
    super.onInit();
  }
}
