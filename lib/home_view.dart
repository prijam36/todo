import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:testbb/home_controller.dart';
import 'package:testbb/model.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final HomeController _controller = Get.put(HomeController());

  String dropdownValue = 'All';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("TODO"),
        actions: [
          DropdownButton<String>(
            value: dropdownValue,
            onChanged: (String? newValue) {
              setState(() {
                dropdownValue = newValue!;
                _controller.filterList(dropdownValue);
                _controller.refreshCounts();
              });
            },
            items: <String>['All', 'Completed', 'Remaining']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ],
      ),
      body: Obx(() => SizedBox(
            child: _controller.loading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TaskCountWidget(
                              count: _controller.completedCount.value,
                              label: 'Completed',
                              color: Colors.green,
                            ),
                            TaskCountWidget(
                              count: _controller.remaining.value,
                              label: 'Remaining',
                              color: Colors.red,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: _controller.filteredList.length,
                          itemBuilder: (context, index) {
                            ToDoModel toDoModel =
                                _controller.filteredList[index];
                            return Dismissible(
                              background: Container(
                                color: Colors.red,
                                child: const Align(
                                    alignment: Alignment.centerRight,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 18.0),
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.white,
                                      ),
                                    )),
                              ),
                              key: UniqueKey(),
                              onDismissed: (direction) {
                                _controller.filteredList.removeAt(index);
                                _controller.refreshCounts();
                              },
                              child: ListTile(
                                leading: Checkbox(
                                  value: toDoModel.completed,
                                  onChanged: (bool? value) {
                                    _controller.checkComplete(value, toDoModel);
                                  },
                                ),
                                title: Text(toDoModel.title ?? 'N/A'),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
          )),
    );
  }
}

class TaskCountWidget extends StatelessWidget {
  final int count;
  final String label;
  final Color color;

  const TaskCountWidget({
    super.key,
    required this.count,
    required this.label,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          count.toString(),
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: color,
          ),
        ),
        const SizedBox(height: 4),
        Text(
          label,
          style: TextStyle(
            fontSize: 16,
            color: color,
          ),
        ),
      ],
    );
  }
}
